#!/usr/bin/env node
const express = require("express");
const { readdir } = require('fs');

const PORT = process.env.PORT || 3001;

const app = express();

app.use(function(req, res, next) {
    const allowedOrigins = ['http://localhost:3000', 'https://unionpotagereversailles.fr'],
          origin = req.headers.origin;
    if (allowedOrigins.includes(origin)) {
        res.header("Access-Control-Allow-Origin", origin); // restrict it to the required domain
    }
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get("/api/", (req, res) => {
    res.json({ message: "Hello from server!" });
});

app.get("/api/imagesPath", (req, res) => {
    readdir('../drive/images', (err, files) => {
        res.json({images: files, err});
    });
});

app.get("/api/eventsPath", (req, res) => {
    readdir('../drive/événements', (err, files) => {
        res.json({events: files.map(name => name.replace(/\.pdf/, ''))});
    });
});

app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
});
