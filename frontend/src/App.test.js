import { render, screen } from '@testing-library/react';
import App from './App';
import {QueryClientProvider, QueryClient} from "@tanstack/react-query";

describe('The App tests', function () {
    window.alert = jest.fn();
    const queryClient = new QueryClient();

    it('Should display the site name', () => {
        render(
            <QueryClientProvider client={queryClient}>
                <App />
            </QueryClientProvider>);
        const linkElement = screen.getByText('Union Potagère de Versailles');
        expect(linkElement).toBeVisible();
    });
});
