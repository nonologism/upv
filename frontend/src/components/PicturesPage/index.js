import {Card, Grid} from '@mui/material';

import './picturesPage.css';
import useImageNames from "../../services/useImageNames";

export default function PicturesPage() {
    const { imageNames } = useImageNames();

    return (
        <>
            <div>
                <h1>
                    Photos de l'union potagère de Versailles
                </h1>
            </div>
            <Grid container justifyContent={'space-around'} alignItems={'center'} sx={{marginTop: '20px'}}>
                {
                    imageNames.map((imageName, index) => (
                        <Grid item key={index} sx={{marginBottom: '20px', overflow: 'hidden'}}>
                            <Card sx={{height: '23vw', width: '23vw', display: 'flex', alignItems: 'center', backgroundColor: 'lightGray'}}>
                                <img className={'galleryImage'} src={require(`../../../../drive/images/${imageName}`)} alt={`${index}`}/>
                            </Card>
                        </Grid>
                    ))
                }
            </Grid>
        </>
    )
}
