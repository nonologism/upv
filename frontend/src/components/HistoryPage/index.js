import {Card, Grid} from '@mui/material';
import photo from '../../resources/images/creation_upv.jpg';

import './historyPage.css'

export default function HistoryPage() {
    return(
        <Grid container sx={{position: 'absolute', height: '100vh'}} alignItems={'center'} justifyContent={'space-evenly'}>
            <Grid item xs={7}>
                <div className={'sketchy-history'}>
                    Allons au jardin ! <br/>
                    Telle était la parole de ralliement des fondateurs de l’association, pour encourager
                    ses membres à défricher petit à petit le terrain situé à droite de la pièce d’eau des
                    Suisses. La création de ces jardins potagers devait aider les familles à se nourrir. <br/><br/>

                    « La brouette du cœur » est une initiative caritative de l’association.<br/><br/>
                    Date de Création de L’union Potagère de Versailles : le 12 octobre 1920. L’association
                    était située 12 rue du Vieux Versailles. En 2020 il y a 120 parcelles situées sur le
                    Domaine du Château de Versailles.<br/><br/>
                    Voici une photo des membres de l’association et de leurs familles, prise en 1920.
                </div>
            </Grid>
            <Grid item sx={{overflow: 'hidden'}}>
                <Card sx={{padding: '1%', width: '98%', display: 'flex', alignItems: 'center', backgroundColor: 'lightGray'}}>
                    <img className={'galleryImage'} src={photo} alt={`creation upv`}/>
                </Card>
            </Grid>
        </Grid>
    )
}