import {forwardRef, useMemo} from 'react'
import {ListItem, ListItemIcon, ListItemText} from '@mui/material';
import {
    Link as RouterLink
} from 'react-router-dom';

export default function ListItemLink(props) {
    const { icon, primary, to } = props;

    const renderLink = useMemo(
        () =>
            forwardRef(function Link(itemProps, ref) {
                return <RouterLink to={to} ref={ref} {...itemProps} role={undefined} />;
            }),
        [to],
    );

    return (
        <li>
            <ListItem button component={renderLink}>
                {icon ? <ListItemIcon>{icon}</ListItemIcon> : null}
                <ListItemText primary={primary} />
            </ListItem>
        </li>
    );
}