import {Collapse, List, ListItemButton, ListItemText} from '@mui/material';
import {ExpandLess, ExpandMore} from '@mui/icons-material';
import ListItemLink from '../../ListItemLink';
import {useState} from 'react';

export default function Submenu({primary, items, path}) {
    const [submenuOpened, toggleSubmenu] = useState(false);

    return (
        <>
            <ListItemButton onClick={() => toggleSubmenu(!submenuOpened)}>
            <ListItemText>{primary}</ListItemText>
            {submenuOpened ? <ExpandLess/> : <ExpandMore/>}
            </ListItemButton>
            <Collapse in={submenuOpened} timeout='auto' unmountOnExit>
            <List component='div' disablePadding sx={{pl: 2}}>
                {items.map((name, index) =>
                    <div key={index}>
                        <ListItemLink to={`${path}${name}`} primary={name}/>
                    </div>
                )}
            </List>
            </Collapse>
        </>
    );
}