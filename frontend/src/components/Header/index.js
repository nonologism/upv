import { useState } from 'react';
import { Outlet } from 'react-router-dom';
import { Drawer, Fab, List } from '@mui/material';
import { Menu } from '@mui/icons-material'


import ListItemLink from '../ListItemLink'
import Submenu from './Submenu';
import axios from "axios";
import {useQuery} from "@tanstack/react-query";

const URL = 'api/eventsPath';

export default function Header() {
    const [menuOpened, toggleMenu] = useState(false),
          { data: events = [] } = useQuery({
              queryKey: ['eventsPath'],
              queryFn: async () => {
                  const { data } = await axios.get(URL);
                  return data.events;
              }
          });

    return (
        <div className={'fullHeight'}>
            <Fab onClick={() => toggleMenu(true)} variant={'extended'} sx={{margin: '20px', position: 'fixed' }}>
                <Menu sx={{paddingBottom: '2px'}}/>
            </Fab>
            <Drawer open={menuOpened} onClose={() => toggleMenu(false)} PaperProps={{sx: {width: '24vh'}}}>
                <List>
                    <ListItemLink to={'/'} primary={'Accueil'} />
                    <Submenu primary={'Évènements'} items={events} path={'events/'}/>
                    <ListItemLink to={'pictures'} primary={'Photos'} />
                    <ListItemLink to={'history'} primary={'Historique'}/>
                    <ListItemLink to={'contact'} primary={'Nous contacter'}/>
                    <ListItemLink to={'ruleBook'} primary={'Règlement intérieur'}/>
                </List>
            </Drawer>
            <Outlet/>
        </div>
    );
}