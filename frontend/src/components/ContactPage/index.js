import {Button, Grid, Paper} from '@mui/material';
import parcelleRequest from '../../resources/files/demande_parcelle.pdf'

function EnhancedPaper({ children }) {
    return (
        <Paper elevation={8} sx={{textAlign: 'center', lineHeight: '30px', padding: '20px', backgroundColor:'lightgrey'}} children={children} />
    );
}

export default function ContactPage() {return (
        <Grid container direction={'column'} alignItems={'center'} justifyContent={'space-evenly'} sx={{height: '100vh'}}>
            <Grid item lg={8}>
                <Button variant={'outlined'} href={parcelleRequest} download sx={{textAlign: 'center'}}>
                    Télécharger le formulaire de demande de parcelle <br/>
                    A envoyer par mail
                </Button>
            </Grid>
            <Grid item lg={8}>
                <EnhancedPaper>
                    mail : unionpotagereversailles@gmail.com
                </EnhancedPaper>
            </Grid>
            <Grid item lg={8}>
                <EnhancedPaper>
                    UNION POTAGERE DE VERSAILLES<br/><br/>
                    12 RUE DE BUC<br/>
                    APPT 1104<br/>
                    LES LOGES EN JOSAS 78350
                </EnhancedPaper>
            </Grid>
            <Grid item lg={8}>
                <EnhancedPaper>
                    TARIF ANNUEL<br/><br/>
                    Cotisation annuelle 0,83 centimes au mètre carré<br/>
                    Frais d’adhésion de 50 € la première année.
                </EnhancedPaper>
            </Grid>
        </Grid>
    )
}