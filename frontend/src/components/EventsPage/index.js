import { useState } from 'react';
import { Document, Page } from 'react-pdf/dist/esm/entry.webpack';

import {Grid} from "@mui/material";

export default function EventsPage({ filename }) {
        const [numPages, setNumPages] = useState(null);

    function onDocumentLoadSuccess({ numPages }) {
        setNumPages(numPages);
    }

    return (
        <Grid container sx={{width: '100vw', paddingTop: '20px'}} alignItems={'center'} direction={'column'}>
            <Document file={require(`../../../../drive/événements/${filename}.pdf`)} onLoadSuccess={onDocumentLoadSuccess}>
                {Array(numPages).fill(0).map((x, i) => (
                    <Grid item key={i} sx={{border: '1px solid'}} alignItems={'center'}>
                        <Page pageNumber={i + 1} renderMode={'svg'} scale={1.5}/>
                    </Grid>
                ))}
            </Document>
        </Grid>
    );
}
