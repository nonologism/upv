import './noPage.css';

export default function NoPage({ message }) {
    return(
        <div className={'noPage'}>
            Désolé, cette page n'existe pas.
            <br/>
            {message}
        </div>
    );
}