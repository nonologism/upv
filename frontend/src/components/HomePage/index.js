import './homePage.css'

import { Link } from 'react-router-dom';

export default function HomePage() {
    return (
        <div>
            <div className={'image'}>
                <div className={'caption'}>
                    <span className={'border'}>Union Potagère de Versailles</span>
                </div>
            </div>
            <div className={'description'}>
                <div className={'descriptionText'}>
                    <p>L’Union Potagère de Versailles est une association de jardins familiaux (anciennement appelés « jardins ouvriers »).</p>
                    <p>Les parcelles sont destinées principalement à la culture du potager.</p>
                    <p>Vous pouvez nous transmettre un dossier de candidature sur la page <Link to={'contact'}>Nous contacter</Link>.</p>
                    <p>Les jardins de l’UPV sont situés à Versailles à côté de la pièce d’eau des Suisses.</p>
                </div>
            </div>
        </div>
    )
}