import {BrowserRouter, Route, Routes} from 'react-router-dom';

import Header from './components/Header';
import HomePage from './components/HomePage';
import PicturesPage from './components/PicturesPage';
import ContactPage from './components/ContactPage';
import HistoryPage from './components/HistoryPage';
import Rulebook from './components/Rulebook';
import { useEffect, useState } from "react";
import axios from "axios";
import EventsPage from "./components/EventsPage";

axios.defaults.baseURL = process.env.NODE_ENV === 'production' ? 'https://unionpotagereversailles.fr/' : 'http://localhost:3001/';

function App() {
  const [events, setEventNames] = useState([]);

  const getAnswer = async () => {
    const { data } = await axios('api/eventsPath');
    setEventNames(data.events);
  }

  useEffect(() => {
    getAnswer().catch(err => alert(err));
  }, []);

  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route element={<Header/>}>
            <Route path={'/'} element={<HomePage/>}/>
            <Route path={'events'} >
              {events.map((name, index) =>
                  <Route path={name.replace(/\.pdf/, '')} key={index} element={<EventsPage filename={name} /> } />
              )}
            </Route>
            <Route path={'pictures'} element={<PicturesPage/>}/>
            <Route path={'history'} element={<HistoryPage/>}/>
            <Route path={'ruleBook'} element={<Rulebook/>}/>
            <Route path={'contact'} element={<ContactPage/>}/>
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
