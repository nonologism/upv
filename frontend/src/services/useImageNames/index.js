import {useQuery} from "@tanstack/react-query";
import axios from "axios";

const URL = '/api/imagesPath';
export default function useImageNames() {
    const { data: imageNames = [] } = useQuery({
            queryKey: ['imageNames'],
            queryFn: async () => {
                const { data } = await axios.get(URL);
                return data.images.map(name => name);
            }
        });

    return {
        imageNames
    };
}
