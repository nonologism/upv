#!/usr/bin/env python3
from __future__ import print_function

import io
import logging
import argparse
import os.path
from inspect import getsourcefile
import re

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaIoBaseDownload
from google.oauth2 import service_account

PATH = re.sub(r"\/driveUpdate\.py", '/', os.path.abspath(getsourcefile(lambda:0)))

if not os.path.exists(PATH + 'drive'):
    os.mkdir(PATH + 'drive')

if not os.path.exists(PATH + 'drive/événements'):
    os.mkdir(PATH + 'drive/événements')
    os.mkdir(PATH + 'drive/images')

# Setup logging
logging.basicConfig(filename=PATH + 'drive/apiScript.log',
                    filemode='a',
                    format='%(asctime)s - %(message)s',
                    level=logging.INFO)

# Connect to google drive api
SCOPES = ['https://www.googleapis.com/auth/drive']

# get credentials through --credentials arg
parser = argparse.ArgumentParser()
parser.add_argument('--credentials', default='')
args = parser.parse_args()
SERVICE_ACCOUNT_FILE = PATH + args.credentials #'upvdatabase-673c40b57ca4.json'
if SERVICE_ACCOUNT_FILE == '':
    logging.error('No credentials specified')

credentials = service_account.Credentials.from_service_account_file(SERVICE_ACCOUNT_FILE, scopes=SCOPES)
service = build('drive', 'v3', credentials=credentials)

# Downloads file with its drive id. relative path with file extension must be specified
def downloadFile(realFileId, path):
    try:
        fileId = realFileId

        request = service.files().get_media(fileId=fileId)
        file = io.BytesIO()
        downloader = MediaIoBaseDownload(file, request)
        done = False
        while done is False:
            status, done = downloader.next_chunk()

    except HttpError as error:
        logging.error(F'An error occurred while trying ot download: {error}')
        file = None

    f = open(path, 'wb')
    f.write(file.getvalue())

# Lists ALL files & folders available through drive api
def listFiles():
    results = service.files().list(pageSize=40, fields='files(id,name,mimeType,parents,md5Checksum)').execute()
    return results.get('files', [])

# Verifies that all files have identical md5checksums. Folders don't have checksums.
def hasDriveContentChanged(items):
    currentChecksumList = []
    checksumList = ['']
    try:
        checksumList = open(PATH + 'drive/lastEdited.txt').read().splitlines()
    except:
        logging.info('Checksum file not yet created.')

    for item in items:
        if item['mimeType'] != 'application/vnd.google-apps.folder':
            currentChecksumList.append(item['md5Checksum'])
    currentChecksumList.sort()

    if checksumList != currentChecksumList:
        file = open(PATH + 'drive/lastEdited.txt', 'w')
        file.write('\n'.join(currentChecksumList))
        return True
    return False


# Downloads files to be displayed in website
items = listFiles()
folderMimeType = 'application/vnd.google-apps.folder'
eventsId = ''
imagesId = ''

if not hasDriveContentChanged(items):
    logging.info('No change detected. Download is unnecessary.')
    exit(0)

# Finds ids of folders (événements & images)
if not items:
    logging.error('Error: no files found.')
    exit(0)
for item in items:
    if item['name'] == 'événements' and item['mimeType'] == folderMimeType:
        eventsId = item['id']
    if item['name'] == 'images' and item['mimeType'] == folderMimeType:
        imagesId = item['id']

if eventsId == '':
    logging.error('Error: events folder not found')
    exit(0)
if imagesId == '':
    logging.error('Error: images folder not found')
    exit(0)

# clear files
folder = PATH + 'drive/images'
for root, dirs, files in os.walk(folder):
    for file in files:
        os.remove(os.path.join(root, file))
folder = PATH + 'drive/événements'
for root, dirs, files in os.walk(folder):
    for file in files:
        os.remove(os.path.join(root, file))

logging.info('Folders cleared, downloading...')

# download from drive
for item in items:
    if item['mimeType'] != folderMimeType:
        if imagesId in item['parents']:
            downloadFile(item['id'], PATH + 'drive/images/' + item['name'])
        if eventsId in item['parents']:
            downloadFile(item['id'], PATH + 'drive/événements/' + item['name'])

logging.info('Download successful')
