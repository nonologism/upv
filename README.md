# UPV

Website for the Union potagère de Versailles

## Run
dev: ```npm start```  
build: ```npm run build```  
backend: ```node backend/index.js```  

## Drive api
This website is using Google Drive as a database.  
`driveUpdate.py` checks for changes in Drive. Said Drive is defined by credentials.
Credentials is a json file that should be passed as an arg of the script.
Then put the script in a cron. [Here](https://developers.google.com/identity/protocols/oauth2/service-account)
is some information on how to get these credentials.

## Backend
```docker run -dp 3001:3001 -it --mount type=bind,src="$(pwd)"/../drive,target=/drive backend-upv```
dp : binds 3001 to 3001
mount bind : container can access drive data

For the cron job to work, you are going to need:  
```sudo pip install google-api-python-client```


Should look like this:  
```<PATH_TO_SCRIPT> --credentials=<RELATIVE_PATH_TO_CREDS>```
